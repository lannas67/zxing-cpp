#!/bin/bash

BUILD=./build

if [ $(uname) = Linux ]; then
	if [ $(uname -m) = armv7l ]; then
		rm -rf $BUILD/arm
	elif [ $(uname -m) = x86_64 ]; then
		rm -rf $BUILD/x64-linux
		rm -rf $BUILD/x64-linux-debug
	else
		rm -rf $BUILD/x86-linux
	fi
else
	rm -rf $BUILD/x64-win
	rm -rf $BUILD/x64-win-debug
fi

. ./build.sh

// -*- mode:c++; tab-width:2; indent-tabs-mode:nil; c-basic-offset:2 -*-
/*
 *  AztecReader.cpp
 *  zxing
 *
 *  Created by Lukas Stabe on 08/02/2012.
 *  Copyright 2012 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <zxing/aztec/AztecReader.h>
#include <zxing/aztec/detector/Detector.h>
#include <zxing/common/DecoderResult.h>
#include <zxing/NotFoundException.h>
#include <iostream>

using zxing::Ref;
using zxing::ArrayRef;
using zxing::Result;
using zxing::aztec::AztecReader;

// VC++
using zxing::BinaryBitmap;
using zxing::DecodeHints;


bool AztecReader::multiSearch = false;
int AztecReader::gridSize = 20; // millimeters
std::vector<Ref<Result> > AztecReader::pageResults;
int AztecReader::pageResultIndex = -1;

void AztecReader::pageReset(bool multiSearch_)
{
	multiSearch = multiSearch_;
	pageResults.clear();
	pageResultIndex = -1;
}


AztecReader::AztecReader() : decoder_() {
  // nothing
}

Ref<Result> AztecReader::decode(Ref<BinaryBitmap> image, DecodeHints hints)
{
	if (pageResultIndex == -1)
	{
		int width  = image->getWidth();
		int height = image->getHeight();
		int gridStep = gridSize / 25.4 * image->getResolution();

		Detector::initX_ = gridStep / 2;
		Detector::initY_ = gridStep / 2;

		while (true)
		{
			while (Detector::initX_ >= width && Detector::initY_ < height)
			{
				Detector::initX_ = gridStep / 2;
				Detector::initY_ += gridStep;
			}

			if (Detector::initY_ >= height)
				break;

			Ref<Result>	result;

			try
			{
				result = doDecode(image, hints);
			}
			catch (ReaderException const& re)
			{
				(void)re;
			}

			if (!multiSearch && result)
				break;

			Detector::initX_ += gridStep;
		}
	}

	if (pageResults.size() == (pageResultIndex + 1))
		throw NotFoundException();
	else
		return pageResults[++pageResultIndex];
}

Ref<Result> AztecReader::decode(Ref<zxing::BinaryBitmap> image)
{
	Detector detector(image->getBlackMatrix());

	Ref<AztecDetectorResult> detectorResult(detector.detect());

	ArrayRef< Ref<ResultPoint> > points(detectorResult->getPoints());

  Ref<DecoderResult> decoderResult(decoder_.decode(detectorResult));

	Ref<Result> result(new Result(decoderResult->getText(), decoderResult->getRawBytes(), points, BarcodeFormat::AZTEC));

	bool alreadyFound = false;
	for (int i = 0; i < pageResults.size(); i++)
	{
		Ref<Result> existingResult = pageResults[i];
		if (existingResult->getText()->getText() == result->getText()->getText())
		{
			alreadyFound = true;
			break;
		}
	}

	if (!alreadyFound)
		pageResults.push_back(result);

	return result;
}

Ref<Result> AztecReader::doDecode(Ref<BinaryBitmap> image, DecodeHints) {
  //cout << "decoding with hints not supported for aztec" << "\n" << flush;
  return this->decode(image);
}

AztecReader::~AztecReader() {
  // nothing
}

zxing::aztec::Decoder& AztecReader::getDecoder() {
  return decoder_;
}

// -*- mode:c++; tab-width:2; indent-tabs-mode:nil; c-basic-offset:2 -*-
/*
 *  DataMatrixReader.cpp
 *  zxing
 *
 *  Created by Luiz Silva on 09/02/2010.
 *  Copyright 2010 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <zxing/datamatrix/DataMatrixReader.h>
#include <zxing/datamatrix/detector/Detector.h>
#include <zxing/NotFoundException.h>
#include <iostream>
#include <sstream>

namespace zxing {
namespace datamatrix {

using namespace std;

bool DataMatrixReader::multiSearch = false;
int DataMatrixReader::gridSize = 20; // millimeters
std::vector<Ref<Result> > DataMatrixReader::pageResults;
int DataMatrixReader::pageResultIndex = -1;

void DataMatrixReader::pageReset(bool multiSearch_)
{
	multiSearch = multiSearch_;
	pageResults.clear();
	pageResultIndex = -1;
}

DataMatrixReader::DataMatrixReader() :
    decoder_() {
}

Ref<Result> DataMatrixReader::decode(Ref<BinaryBitmap> image, DecodeHints hints)
{
	if (pageResultIndex == -1)
	{
		int width  = image->getWidth();
		int height = image->getHeight();
		int gridStep = gridSize / 25.4 * image->getResolution();

		int sign = 1;
		float thresholdRatio = 1;
		for (int i = 0; i < 5; i++)
		{
			sign *= -1;
			thresholdRatio += (sign * i * 0.1);
			for (int j = 0; j < 2; j++)
			{
				int attempt = 2 * i + j + 1;
				bool fullStep = (j == 0);

				Detector::initX_ = gridStep / 2;
				Detector::initY_ = gridStep / 2;

				while (true)
				{
					while (Detector::initX_ >= width && Detector::initY_ < height)
					{
						Detector::initX_ = gridStep / 2;
						Detector::initY_ += gridStep;
					}

					if (Detector::initY_ >= height)
						break;

					Ref<Result>	result;

					try
					{
						result = doDecode(image, hints, fullStep, thresholdRatio, attempt);
					}
					catch (ReaderException const& re)
					{
						(void)re;
					}

					if (!multiSearch && result)
						break;

					Detector::initX_ += gridStep;
				}

				// If nothing is found and trying harder, let's try again.
				if (pageResults.size() || !hints.getTryHarder())
					break;
			}

			// If nothing is found and trying harder, let's try again.
			if (pageResults.size() || !hints.getTryHarder())
				break;
		}
	}

	if (pageResults.size() == (pageResultIndex + 1))
		throw NotFoundException();
	else
		return pageResults[++pageResultIndex];
}

Ref<Result> DataMatrixReader::doDecode(Ref<BinaryBitmap> image, DecodeHints hints, bool fullStep, float thresholdRatio, int attempt){
	(void)hints;
	Detector detector(image->getBlackMatrix(thresholdRatio));
	Ref<DetectorResult> detectorResult(detector.detect(fullStep));
	ArrayRef< Ref<ResultPoint> > points(detectorResult->getPoints());

	Ref<DecoderResult> decoderResult(decoder_.decode(detectorResult->getBits()));

	ostringstream remarksStream;
	remarksStream << "Attempt " << attempt;
	Ref<String> remarks(new String(remarksStream.str()));

	Ref<Result> result(new Result(decoderResult->getText(), decoderResult->getRawBytes(), points, BarcodeFormat::DATA_MATRIX, remarks));

	bool alreadyFound = false;
	for (int i = 0; i < pageResults.size(); i++)
	{
		Ref<Result> existingResult = pageResults[i];
		if (existingResult->getText()->getText() == result->getText()->getText())
		{
			alreadyFound = true;
			break;
		}
	}

	if (!alreadyFound)
		pageResults.push_back(result);

	return result;
}

DataMatrixReader::~DataMatrixReader() {
}

}
}

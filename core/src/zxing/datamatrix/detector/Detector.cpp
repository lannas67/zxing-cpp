// -*- mode:c++; tab-width:2; indent-tabs-mode:nil; c-basic-offset:2 -*-
/*
 *  Detector.cpp
 *  zxing
 *
 *  Created by Luiz Silva on 09/02/2010.
 *  Copyright 2010 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <map>
#include <zxing/ResultPoint.h>
#include <zxing/common/GridSampler.h>
#include <zxing/datamatrix/detector/Detector.h>
#include <zxing/datamatrix/Version.h>
#include <zxing/common/detector/MathUtils.h>
#include <zxing/NotFoundException.h>
#include <sstream>
#include <cstdlib>
#include <algorithm>

using std::abs;
using zxing::Ref;
using zxing::BitMatrix;
using zxing::ResultPoint;
using zxing::DetectorResult;
using zxing::PerspectiveTransform;
using zxing::NotFoundException;
using zxing::datamatrix::Detector;
using zxing::datamatrix::ResultPointsAndTransitions;
using zxing::common::detector::MathUtils;

namespace {
  typedef std::map<Ref<ResultPoint>, int> PointMap;
  void increment(PointMap& table, Ref<ResultPoint> const& key) {
    int& value = table[key];
    value += 1;
  }
}

ResultPointsAndTransitions::ResultPointsAndTransitions() {
  Ref<ResultPoint> ref(new ResultPoint(0, 0));
  from_ = ref;
  to_ = ref;
  transitions_ = 0;
}

ResultPointsAndTransitions::ResultPointsAndTransitions(Ref<ResultPoint> from, Ref<ResultPoint> to,
                                                       int transitions)
  : to_(to), from_(from), transitions_(transitions) {
}

Ref<ResultPoint> ResultPointsAndTransitions::getFrom() {
  return from_;
}

Ref<ResultPoint> ResultPointsAndTransitions::getTo() {
  return to_;
}

int ResultPointsAndTransitions::getTransitions() {
  return transitions_;
}



int Detector::initX_ = 0;
int Detector::initY_ = 0;
int Detector::initSize_ = 60;

Detector::Detector(Ref<BitMatrix> image)
  : image_(image) {
}

Ref<BitMatrix> Detector::getImage() {
  return image_;
}

Ref<DetectorResult> Detector::detect(bool fullStep)
{
	Ref<WhiteRectangleDetector> rectangleDetector_(new WhiteRectangleDetector(image_, initSize_, initX_, initY_));
	std::vector<Ref<ResultPoint> > ResultPoints = rectangleDetector_->detect();
	correctRectangle(ResultPoints, fullStep);

	Ref<ResultPoint> pointA = ResultPoints[0];
	Ref<ResultPoint> pointB = ResultPoints[1];
	Ref<ResultPoint> pointC = ResultPoints[2];
	Ref<ResultPoint> pointD = ResultPoints[3];

	// Point A and D are across the diagonal from one another,
	// as are B and C. Figure out which are the solid black lines
	// by counting transitions
	std::vector<Ref<ResultPointsAndTransitions> > transitions(4);
	transitions[0].reset(transitionsBetween(pointA, pointB));
	transitions[1].reset(transitionsBetween(pointA, pointC));
	transitions[2].reset(transitionsBetween(pointB, pointD));
	transitions[3].reset(transitionsBetween(pointC, pointD));

	insertionSort(transitions);

	// Sort by number of transitions. First two will be the two solid sides; last two
	// will be the two alternating black/white sides
	Ref<ResultPointsAndTransitions> lSideOne(transitions[0]);
	Ref<ResultPointsAndTransitions> lSideTwo(transitions[1]);

	// Figure out which point is their intersection by tallying up the number of times we see the
	// endpoints in the four endpoints. One will show up twice.
	typedef std::map<Ref<ResultPoint>, int> PointMap;
	PointMap pointCount;
	increment(pointCount, lSideOne->getFrom());
	increment(pointCount, lSideOne->getTo());
	increment(pointCount, lSideTwo->getFrom());
	increment(pointCount, lSideTwo->getTo());

	// Figure out which point is their intersection by tallying up the number of times we see the
	// endpoints in the four endpoints. One will show up twice.
	Ref<ResultPoint> maybeTopLeft;
	Ref<ResultPoint> bottomLeft;
	Ref<ResultPoint> maybeBottomRight;
	for (PointMap::const_iterator entry = pointCount.begin(), end = pointCount.end(); entry != end; ++entry) {
		Ref<ResultPoint> const& point = entry->first;
		int value = entry->second;
		if (value == 2) {
		  bottomLeft = point; // this is definitely the bottom left, then -- end of two L sides
		} else {
		  // Otherwise it's either top left or bottom right -- just assign the two arbitrarily now
		  if (maybeTopLeft == 0) {
			maybeTopLeft = point;
		  } else {
			maybeBottomRight = point;
		  }
		}
	}

	if (maybeTopLeft == 0 || bottomLeft == 0 || maybeBottomRight == 0) {
		throw NotFoundException();
	}

	// Bottom left is correct but top left and bottom right might be switched
	std::vector<Ref<ResultPoint> > corners(3);
	corners[0].reset(maybeTopLeft);
	corners[1].reset(bottomLeft);
	corners[2].reset(maybeBottomRight);

	// Use the dot product trick to sort them out
	ResultPoint::orderBestPatterns(corners);

	// Now we know which is which:
	Ref<ResultPoint> bottomRight(corners[0]);
	bottomLeft = corners[1];
	Ref<ResultPoint> topLeft(corners[2]);

	// Which point didn't we find in relation to the "L" sides? that's the top right corner
	Ref<ResultPoint> topRight;
	if (!(pointA->equals(bottomRight) || pointA->equals(bottomLeft) || pointA->equals(topLeft))) {
		topRight = pointA;
	} else if (!(pointB->equals(bottomRight) || pointB->equals(bottomLeft)
			   || pointB->equals(topLeft))) {
		topRight = pointB;
	} else if (!(pointC->equals(bottomRight) || pointC->equals(bottomLeft)
			   || pointC->equals(topLeft))) {
		topRight = pointC;
	} else {
		topRight = pointD;
	}

	// Next determine the dimension by tracing along the top or right side and counting black/white
	// transitions. Since we start inside a black module, we should see a number of transitions
	// equal to 1 less than the code dimension. Well, actually 2 less, because we are going to
	// end on a black module:

	// The top right point is actually the corner of a module, which is one of the two black modules
	// adjacent to the white module at the top right. Tracing to that corner from either the top left
	// or bottom right should work here.

	int dimensionTop = transitionsBetween(topLeft, topRight)->getTransitions();
	int dimensionRight = transitionsBetween(bottomRight, topRight)->getTransitions();

	dimensionTop++;
	dimensionRight++;

	correctDimensions(dimensionTop, dimensionRight);

	Ref<PerspectiveTransform> transform = createTransform(topLeft, topRight, bottomLeft, bottomRight, dimensionTop, dimensionRight);

	Ref<BitMatrix> bits = sampleGrid(image_, dimensionTop, dimensionRight, transform);

	ArrayRef< Ref<ResultPoint> > points (new Array< Ref<ResultPoint> >(4));
	points[0].reset(topLeft);
	points[1].reset(bottomLeft);
	points[2].reset(topRight);
	points[3].reset(bottomRight);

	Ref<DetectorResult> detectorResult(new DetectorResult(bits, points));

	return detectorResult;
}

/**
 * Calculates the position of the white top right module using the output of the rectangle detector
 * for a rectangular matrix
 */
Ref<ResultPoint> Detector::correctTopRightRectangular(Ref<ResultPoint> bottomLeft,
                                                      Ref<ResultPoint> bottomRight, Ref<ResultPoint> topLeft, Ref<ResultPoint> topRight,
                                                      int dimensionTop, int dimensionRight) {

  float corr = distance(bottomLeft, bottomRight) / (float) dimensionTop;
  int norm = distance(topLeft, topRight);
  float cos = (topRight->getX() - topLeft->getX()) / norm;
  float sin = (topRight->getY() - topLeft->getY()) / norm;

  Ref<ResultPoint> c1(
    new ResultPoint(topRight->getX() + corr * cos, topRight->getY() + corr * sin));

  corr = distance(bottomLeft, topLeft) / (float) dimensionRight;
  norm = distance(bottomRight, topRight);
  cos = (topRight->getX() - bottomRight->getX()) / norm;
  sin = (topRight->getY() - bottomRight->getY()) / norm;

  Ref<ResultPoint> c2(
    new ResultPoint(topRight->getX() + corr * cos, topRight->getY() + corr * sin));

  if (!isValid(c1)) {
    if (isValid(c2)) {
      return c2;
    }
    return Ref<ResultPoint>(NULL);
  }
  if (!isValid(c2)) {
    return c1;
  }

  int l1 = abs(dimensionTop - transitionsBetween(topLeft, c1)->getTransitions())
    + abs(dimensionRight - transitionsBetween(bottomRight, c1)->getTransitions());
  int l2 = abs(dimensionTop - transitionsBetween(topLeft, c2)->getTransitions())
    + abs(dimensionRight - transitionsBetween(bottomRight, c2)->getTransitions());

  return l1 <= l2 ? c1 : c2;
}

/**
 * Calculates the position of the white top right module using the output of the rectangle detector
 * for a square matrix
 */
Ref<ResultPoint> Detector::correctTopRight(Ref<ResultPoint> bottomLeft,
                                           Ref<ResultPoint> bottomRight, Ref<ResultPoint> topLeft, Ref<ResultPoint> topRight,
                                           int dimension) {

  float corr = distance(bottomLeft, bottomRight) / (float) dimension;
  int norm = distance(topLeft, topRight);
  float cos = (topRight->getX() - topLeft->getX()) / norm;
  float sin = (topRight->getY() - topLeft->getY()) / norm;

  Ref<ResultPoint> c1(
    new ResultPoint(topRight->getX() + corr * cos, topRight->getY() + corr * sin));

  corr = distance(bottomLeft, topLeft) / (float) dimension;
  norm = distance(bottomRight, topRight);
  cos = (topRight->getX() - bottomRight->getX()) / norm;
  sin = (topRight->getY() - bottomRight->getY()) / norm;

  Ref<ResultPoint> c2(
    new ResultPoint(topRight->getX() + corr * cos, topRight->getY() + corr * sin));

  if (!isValid(c1)) {
    if (isValid(c2)) {
      return c2;
    }
    return Ref<ResultPoint>(NULL);
  }
  if (!isValid(c2)) {
    return c1;
  }

  int l1 = abs(
    transitionsBetween(topLeft, c1)->getTransitions()
    - transitionsBetween(bottomRight, c1)->getTransitions());
  int l2 = abs(
    transitionsBetween(topLeft, c2)->getTransitions()
    - transitionsBetween(bottomRight, c2)->getTransitions());

  return l1 <= l2 ? c1 : c2;
}

bool Detector::isValid(Ref<ResultPoint> p) {
  return p->getX() >= 0 && p->getX() < image_->getWidth() && p->getY() > 0
    && p->getY() < image_->getHeight();
}

int Detector::distance(Ref<ResultPoint> a, Ref<ResultPoint> b) {
  return MathUtils::round(ResultPoint::distance(a, b));
}

Ref<ResultPointsAndTransitions> Detector::transitionsBetween(Ref<ResultPoint> from,
                                                             Ref<ResultPoint> to) {
  // See QR Code Detector, sizeOfBlackWhiteBlackRun()
  int fromX = (int) from->getX();
  int fromY = (int) from->getY();
  int toX = (int) to->getX();
  int toY = (int) to->getY();
  bool steep = abs(toY - fromY) > abs(toX - fromX);
  if (steep) {
    int temp = fromX;
    fromX = fromY;
    fromY = temp;
    temp = toX;
    toX = toY;
    toY = temp;
  }

  int dx = abs(toX - fromX);
  int dy = abs(toY - fromY);
  int error = -dx / 2;
  int ystep = fromY < toY ? 1 : -1;
  int xstep = fromX < toX ? 1 : -1;
  int transitions = 0;
  bool inBlack = image_->get(steep ? fromY : fromX, steep ? fromX : fromY);
  for (int x = fromX, y = fromY; x != toX; x += xstep) {
    bool isBlack = image_->get(steep ? y : x, steep ? x : y);
    if (isBlack != inBlack) {
      transitions++;
      inBlack = isBlack;
    }
    error += dy;
    if (error > 0) {
      if (y == toY) {
        break;
      }
      y += ystep;
      error -= dx;
    }
  }
  Ref<ResultPointsAndTransitions> result(new ResultPointsAndTransitions(from, to, transitions));
  return result;
}

Ref<PerspectiveTransform> Detector::createTransform(Ref<ResultPoint> topLeft,
                                                    Ref<ResultPoint> topRight, Ref<ResultPoint> bottomLeft, Ref<ResultPoint> bottomRight,
                                                    int dimensionX, int dimensionY) {

  Ref<PerspectiveTransform> transform(
    PerspectiveTransform::quadrilateralToQuadrilateral(
      0.5f,
      0.5f,
      dimensionX - 0.5f,
      0.5f,
      dimensionX - 0.5f,
      dimensionY - 0.5f,
      0.5f,
      dimensionY - 0.5f,
      topLeft->getX(),
      topLeft->getY(),
      topRight->getX(),
      topRight->getY(),
      bottomRight->getX(),
      bottomRight->getY(),
      bottomLeft->getX(),
      bottomLeft->getY()));
  return transform;
}

Ref<BitMatrix> Detector::sampleGrid(Ref<BitMatrix> image, int dimensionX, int dimensionY,
                                    Ref<PerspectiveTransform> transform) {
  GridSampler &sampler = GridSampler::getInstance();
  return sampler.sampleGrid(image, dimensionX, dimensionY, transform);
}

void Detector::insertionSort(std::vector<Ref<ResultPointsAndTransitions> > &vector) {
  int max = vector.size();
  bool swapped = true;
  Ref<ResultPointsAndTransitions> value;
  Ref<ResultPointsAndTransitions> valueB;
  do {
    swapped = false;
    for (int i = 1; i < max; i++) {
      value = vector[i - 1];
      if (compare(value, (valueB = vector[i])) > 0){
        swapped = true;
        vector[i - 1].reset(valueB);
        vector[i].reset(value);
      }
    }
  } while (swapped);
}

int Detector::compare(Ref<ResultPointsAndTransitions> a, Ref<ResultPointsAndTransitions> b) {
  return a->getTransitions() - b->getTransitions();
}

void Detector::correctRectangle(std::vector<Ref<ResultPoint> >& points, bool fullStep)
{
	/*
	      d
		0----2
	  a |    | c
		1----3
	       b
	*/
    float x0 = points[0]->getX();
    float y0 = points[0]->getY();
    float x1 = points[1]->getX();
    float y1 = points[1]->getY();
    float x2 = points[2]->getX();
    float y2 = points[2]->getY();
    float x3 = points[3]->getX();
    float y3 = points[3]->getY();

    // segment lenghts
    float la = sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0));
    float lb = sqrt((x1 - x3) * (x1 - x3) + (y1 - y3) * (y1 - y3));
    float lc = sqrt((x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3));
    float ld = sqrt((x2 - x0) * (x2 - x0) + (y2 - y0) * (y2 - y0));

    float t, c, s;
    if (fabs(la - lc) > fabs(lb - ld))
	{
		Ref<ResultPointsAndTransitions> transitions02 = transitionsBetween(points[0], points[2]);
		Ref<ResultPointsAndTransitions> transitions13 = transitionsBetween(points[1], points[3]);

		if (la > lc)
		{
			float dist = (fullStep ? la : (la + lc) / 2);
			if (transitions02->getTransitions() > transitions13->getTransitions())
			{
				// move 2 over c
				movePointOverSegment(x3, y3, x2, y2, dist);
			}
			else
			{
				// move 3 over c
				movePointOverSegment(x2, y2, x3, y3, dist);
			}
		}
		else
		{
			float dist = (fullStep ? lc : (la + lc) / 2);
			if (transitions02->getTransitions() > transitions13->getTransitions())
			{
				// move 0 over a
				movePointOverSegment(x1, y1, x0, y0, dist);
			}
			else
			{
				// move 1 over a
				movePointOverSegment(x0, y0, x1, y1, dist);
			}
		}
	}
	else
	{
		Ref<ResultPointsAndTransitions> transitions01 = transitionsBetween(points[0], points[1]);
		Ref<ResultPointsAndTransitions> transitions23 = transitionsBetween(points[2], points[3]);

		if (lb > ld)
		{
			float dist = (fullStep ? lb : (lb + ld) / 2);
			if (transitions01->getTransitions() > transitions23->getTransitions())
			{
				// move 0 over d
				movePointOverSegment(x2, y2, x0, y0, dist);
			}
			else
			{
				// move 2 over d
				movePointOverSegment(x0, y0, x2, y2, dist);
			}
		}
		else
		{
			float dist = (fullStep ? ld : (lb + ld) / 2);
			if (transitions01->getTransitions() > transitions23->getTransitions())
			{
				// move 1 over b
				movePointOverSegment(x3, y3, x1, y1, dist);
			}
			else
			{
				// move 3 over b
				movePointOverSegment(x1, y1, x3, y3, dist);
			}
		}
	}

	points[0] = new ResultPoint(x0, y0);
	points[1] = new ResultPoint(x1, y1);
	points[2] = new ResultPoint(x2, y2);
	points[3] = new ResultPoint(x3, y3);

    return;
}

void Detector::movePointOverSegment(float x0, float y0, float& x1, float& y1, float dist)
{
	float t, c, s;

	if (x1 != x0)
	{
		t = (y1 - y0) / (x1 - x0);                 // tan(alpha)
		c = (x1 > x0 ? 1 : -1) / sqrt(1 + t * t);  // cos(alpha)
		s = t * c;                                 // sin(alpha)
	}
	else
	{
		if (y1 == y0)
			return; // SHOULD THROW AN EXCEPTION!

		c = 0, s = (y1 > y0 ? 1 : -1);
	}

	x1 = x0 + c * dist;
	y1 = y0 + s * dist;
}

void Detector::correctDimensions(int& top, int& right)
{
	std::vector<Ref<Version> >& versions =  Version::VERSIONS;

	int bestIndex = -1;
	int bestError = 1000;
	for (int i = 0; i < versions.size(); i++)
	{
		int error = abs(versions[i]->getSymbolSizeRows() - right) + abs(versions[i]->getSymbolSizeColumns() - top);
		if (error <= bestError) // round by excess
		{
			bestError = error;
			bestIndex = i;
		}
	}

	if (bestIndex < 0)
		return;

	top   = versions[bestIndex]->getSymbolSizeColumns();
	right = versions[bestIndex]->getSymbolSizeRows();
}

// -*- mode:c++; tab-width:2; indent-tabs-mode:nil; c-basic-offset:2 -*-
/*
 *  Copyright 2010 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <zxing/ZXing.h>
#include <zxing/oned/OneDReader.h>
#include <zxing/ReaderException.h>
#include <zxing/oned/OneDResultPoint.h>
#include <zxing/NotFoundException.h>
#include <math.h>
#include <limits.h>
#include <algorithm>
#include <cfloat>

using std::vector;
using zxing::Ref;
using zxing::Result;
using zxing::NotFoundException;
using zxing::oned::OneDReader;

// VC++
using zxing::BinaryBitmap;
using zxing::BitArray;
using zxing::DecodeHints;


bool OneDReader::multiSearch = false;
bool OneDReader::rotated = false;
std::vector<Ref<Result> > OneDReader::pageResults;
int OneDReader::pageResultIndex = -1;
std::vector<Ref<Result> > OneDReader::rowResults;
int OneDReader::rowResultIndex = -1;

void OneDReader::pageReset(bool multiSearch_)
{
	multiSearch = multiSearch_;
	pageResults.clear();
	pageResultIndex = -1;
}

void OneDReader::rowReset()
{
	rowResults.clear();
	rowResultIndex = -1;
}

OneDReader::OneDReader() {}

Ref<Result> OneDReader::decode(Ref<BinaryBitmap> image, DecodeHints hints)
{
	if (pageResultIndex == -1)
	{
		try
		{
			rotated = false;
			doDecode(image, hints);
		}
		catch(NotFoundException const& nfe)
		{
			(void)nfe;
		}

		if (image->isRotateSupported())
		{
			Ref<BinaryBitmap> rotatedImage(image->rotateCounterClockwise());

			try
			{
				rotated = true;
				doDecode(rotatedImage, hints);
			}
			catch(NotFoundException const& nfe)
			{
				(void)nfe;
			}
		}
	}

	if (pageResults.size() == (pageResultIndex + 1))
		throw NotFoundException();
	else
		return pageResults[++pageResultIndex];
}

//#include <typeinfo>

Ref<Result> OneDReader::doDecode(Ref<BinaryBitmap> image, DecodeHints hints)
{
	int width = image->getWidth();
	int height = image->getHeight();
	int resolution = image->getResolution();

	Ref<BitArray> row(new BitArray(width));

	int rowStep = int(3.5 / 25.4 * resolution); // 3.5mm minimum

	bool atLeastOneFound = false;

	for (int x = 1; x <= height; x++)
	{
		if (!multiSearch && atLeastOneFound)
			break;

		int rowNumber = rowStep * x;

		if (rowNumber < 0 || rowNumber >= height)
			break;

		// Estimate black point for this row and load it:
		try
		{
			row = image->getBlackRow(rowNumber, row);
		}
		catch (NotFoundException const& ignored)
		{
			(void)ignored;
			continue;
		}

		// While we have the image data in a BitArray, it's fairly cheap to reverse it in place to
		// handle decoding upside down barcodes.
		for (int attempt = 0; attempt < 2; attempt++)
		{
			if (!multiSearch && atLeastOneFound)
				break;

			if (attempt == 1)
				row->reverse(); // reverse the row and continue

			try
			{
				rowReset();

				while (multiSearch || !atLeastOneFound)
				{
					// Look for a barcode
					// std::cerr << "rn " << rowNumber << " " << typeid(*this).name() << std::endl;
					Ref<Result> result = decodeRow(rowNumber, row);

					// We found our barcode
					if (attempt == 1)
					{
						// But it was upside down, so note that
						// result.putMetadata(ResultMetadataType.ORIENTATION, new Integer(180));
						// And remember to flip the result points horizontally.
						ArrayRef< Ref<ResultPoint> > points(result->getResultPoints());
						if (points)
						{
							points[0] = Ref<ResultPoint>(new OneDResultPoint(width - points[0]->getX() - 1, points[0]->getY()));
							points[1] = Ref<ResultPoint>(new OneDResultPoint(width - points[1]->getX() - 1, points[1]->getY()));
						}
					}

					if (rotated)
					{
						ArrayRef< Ref<ResultPoint> > points = result->getResultPoints();
						if (points && !points->empty())
						{
							for (int i = 0; i < points->size(); i++)
								points[i].reset(new OneDResultPoint(height - points[i]->getY() - 1, points[i]->getX()));
						}
					}

					bool alreadyFound = false;
					for (int i = 0; i < pageResults.size(); i++)
					{
						Ref<Result> existingResult = pageResults[i];
						if (existingResult->getText()->getText() == result->getText()->getText())
						{
							alreadyFound = true;
							break;
						}
					}

					if (!alreadyFound)
						pageResults.push_back(result);

					atLeastOneFound = true;
				}
			}
			catch (ReaderException const& re)
			{
				(void)re;
				continue;
			}
		}
	}

	return Ref<Result>();
}

float OneDReader::patternMatchVariance(vector<int>& counters,
                                       vector<int> const& pattern,
                                       float maxIndividualVariance) {
  return patternMatchVariance(counters, &pattern[0], maxIndividualVariance);
}

float OneDReader::patternMatchVariance(vector<int>& counters,
                                     int const pattern[],
                                     float maxIndividualVariance) {
  int numCounters = counters.size();
  unsigned int total = 0;
  unsigned int patternLength = 0;
  for (int i = 0; i < numCounters; i++) {
    total += counters[i];
    patternLength += pattern[i];
  }
  if (total < patternLength) {
    // If we don't even have one pixel per unit of bar width, assume this is too small
    // to reliably match, so fail:
    return FLT_MAX;
  }

  float unitBarWidth = (float) total / patternLength;
  maxIndividualVariance *= unitBarWidth;

  float totalVariance = 0.0f;
  for (int x = 0; x < numCounters; x++) {
    int counter = counters[x];
    float scaledPattern = pattern[x] * unitBarWidth;
    float variance = counter > scaledPattern ? counter - scaledPattern : scaledPattern - counter;
    if (variance > maxIndividualVariance) {
      return FLT_MAX;
    }
    totalVariance += variance;
  }
  return totalVariance / total;
}

void OneDReader::recordPattern(Ref<BitArray> row,
                               int start,
                               vector<int>& counters) {
  int numCounters = counters.size();
  for (int i = 0; i < numCounters; i++) {
    counters[i] = 0;
  }
  int end = row->getSize();
  if (start >= end) {
    throw NotFoundException();
  }
  bool isWhite = !row->get(start);
  int counterPosition = 0;
  int i = start;
  while (i < end) {
    if (row->get(i) ^ isWhite) { // that is, exactly one is true
      counters[counterPosition]++;
    } else {
      counterPosition++;
      if (counterPosition == numCounters) {
        break;
      } else {
        counters[counterPosition] = 1;
        isWhite = !isWhite;
      }
    }
    i++;
  }
  // If we read fully the last section of pixels and filled up our counters -- or filled
  // the last counter but ran off the side of the image, OK. Otherwise, a problem.
  if (!(counterPosition == numCounters || (counterPosition == numCounters - 1 && i == end))) {
    throw NotFoundException();
  }
}

OneDReader::~OneDReader() {}

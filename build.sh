#!/bin/bash

BUILD=./build

mkdir $BUILD

if [ $(uname) = Linux ]; then
	if [ $(uname -m) = armv7l ]; then
		mkdir $BUILD/arm
		pushd $BUILD/arm
		cmake -G "Unix Makefiles" -DMICROREI_TARGET_ARCH=arm ../..
		make > make.log 2>&1
		popd
	elif [ $(uname -m) = x86_64 ]; then
		mkdir $BUILD/x64-linux
		pushd $BUILD/x64-linux
		cmake -G "Unix Makefiles" -DMICROREI_TARGET_ARCH=x64-linux ../..
		make > make.log 2>&1
		popd

		mkdir $BUILD/x64-linux-debug
		pushd $BUILD/x64-linux-debug
		cmake -G "Unix Makefiles" -DMICROREI_TARGET_ARCH=x64-linux-debug ../..
		make > make.log 2>&1
		popd
	else
		mkdir $BUILD/x86-linux
		pushd $BUILD/x86-linux
		cmake -G "Unix Makefiles" -DMICROREI_TARGET_ARCH=x86-linux ../..
		make > make.log 2>&1
		popd
	fi
else
	mkdir $BUILD/x64-win
	pushd $BUILD/x64-win
	cmake -G "Unix Makefiles" -DMICROREI_TARGET_ARCH=x64-win ../..
	make > make.log 2>&1
	popd

	mkdir $BUILD/x64-win-debug
	pushd $BUILD/x64-win-debug
	cmake -G "Unix Makefiles" -DMICROREI_TARGET_ARCH=x64-win-debug ../..
	make > make.log 2>&1
	popd
fi

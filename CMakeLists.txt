#
# CMake listfile to specify the build process, see:
# http://www.cmake.org/cmake/help/documentation.html
#
project(zxing)
cmake_minimum_required(VERSION 3.0.0)

set(CMAKE_LIBRARY_PATH /opt/local/lib ${CMAKE_LIBRARY_PATH})

# Check for polluted source tree.
if(EXISTS ${CMAKE_SOURCE_DIR}/CMakeCache.txt OR
	EXISTS ${CMAKE_SOURCE_DIR}/CMakeFiles)
	message(FATAL_ERROR
		"Source directory is polluted:"
		"\n  * remove CMakeCache.txt"
		"\n  * remove CMakeFiles directory")
endif()

# Suppress in-source builds.
if(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
	message(FATAL_ERROR
		"CMake generation is not allowed within the source directory:"
		"\n  * mkdir build"
		"\n  * cd build"
		"\n  * Unix-like: cmake -G \"Unix Makefiles\" .."
		"\n  * Windows: cmake -G \"Visual Studio 10\" ..")
endif()

# Adjust CMake's module path.
set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake/)

# Suppress MSVC CRT warnings.
if(MSVC)
	add_definitions(-D_CRT_SECURE_NO_WARNINGS)
	add_definitions(/Za)
endif()

## Detect the target architecture. #############################################
if(${MICROREI_TARGET_ARCH} STREQUAL "arm")
	message("arm target architecture - native compilation")
	set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -fPIC")
elseif(${MICROREI_TARGET_ARCH} STREQUAL "x64-linux")
	message("x64-linux target architecture")
	set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -m64 -fPIC")
	set(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} -m64")
elseif(${MICROREI_TARGET_ARCH} STREQUAL "x64-linux-debug")
	message("x64-linux-debug target architecture")
	set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -g -m64 -fPIC")
	set(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} -m64")
elseif(${MICROREI_TARGET_ARCH} STREQUAL "x86-linux")
	message("x86-linux target architecture")
	set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -m32")
	set(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} -m32")
elseif(${MICROREI_TARGET_ARCH} STREQUAL "x64-win")
	message("x64-win target architecture")
	set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -m64 -fPIC")
	set(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} -m64")
elseif(${MICROREI_TARGET_ARCH} STREQUAL "x64-win-debug")
	message("x64-win-debug target architecture")
	set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -g -m64 -fPIC")
	set(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} -m64")
else()
	message(FATAL_ERROR "WRONG TARGET ARCHITECTURE")
endif()

if(${MICROREI_TARGET_ARCH} STREQUAL "x64-linux-debug" OR ${MICROREI_TARGET_ARCH} STREQUAL "x64-win-debug")
	set(LIBZXING libzxingd)
else()
	set(LIBZXING libzxing)
endif()
################################################################################

# Add libzxing library.
file(GLOB_RECURSE LIBZXING_FILES
	"./core/src/*.cpp"
	"./core/src/*.h"
	"./core/src/*.cc"
	"./core/src/*.hh"
)
if(WIN32)
	file(GLOB LIBZXING_WIN32_FILES
		"./core/lib/win32/*.c"
		"./core/lib/win32/*.h"
	)
	set(LIBZXING_FILES ${LIBZXING_FILES} ${LIBZXING_WIN32_FILES})
	include_directories(SYSTEM "./core/lib/win32/")
endif()

# OpenCV classes
#find_package(OpenCV)
#if(OpenCV_FOUND)
#	file(GLOB_RECURSE LIBZXING_OPENCV_FILES
#		"./opencv/src/*.cpp"
#		"./opencv/src/*.h"
#	)
#	set(LIBZXING_FILES ${LIBZXING_FILES} ${LIBZXING_OPENCV_FILES})
#	include_directories(${OpenCV_INCLUDE_DIRS})
#	include_directories("./opencv/src/")
#endif()

include_directories("./core/src/")
add_library(${LIBZXING} ${LIBZXING_FILES})
set_target_properties(${LIBZXING} PROPERTIES PREFIX "")

#find_package(Iconv)
#if(ICONV_FOUND)
#	include_directories(${ICONV_INCLUDE_DIR})
#	target_link_libraries(${LIBZXING} ${ICONV_LIBRARIES})
#else()
	add_definitions(-DNO_ICONV=1)
#endif()

# Add OpenCV cli executable
#if(OpenCV_FOUND)
#    file(GLOB_RECURSE OPENCV_ZXING_FILES
#        "./opencv-cli/src/*.cpp"
#        "./opencv-cli/src/*.h"
#    )
#    add_executable(zxing-cv ${OPENCV_ZXING_FILES})
#    target_link_libraries(zxing-cv ${LIBZXING} ${OpenCV_LIBRARIES})
#endif()

# Add cli executable.
file(GLOB_RECURSE ZXING_FILES
	"./cli/src/*.cpp"
	"./cli/src/*.h"
)

add_executable(zxing ${ZXING_FILES})
target_link_libraries(zxing ${LIBZXING})
install(TARGETS zxing ${LIBZXING}
	LIBRARY DESTINATION lib
	RUNTIME DESTINATION bin
	ARCHIVE DESTINATION lib)
install(DIRECTORY core/src/zxing/ DESTINATION include/zxing FILES_MATCHING PATTERN "*.h")

# Add testrunner executable.
find_package(CPPUNIT)
if(CPPUNIT_FOUND)
	file(GLOB_RECURSE TESTRUNNER_FILES
		"./core/tests/src/*.cpp"
		"./core/tests/src/*.h"
	)
	add_executable(testrunner ${TESTRUNNER_FILES})
	include_directories(${CPPUNIT_INCLUDE_DIR})
	target_link_libraries(testrunner ${LIBZXING}  ${CPPUNIT_LIBRARIES})
else()
	message(WARNING "Not building testrunner, because CppUnit is missing")
endif()
